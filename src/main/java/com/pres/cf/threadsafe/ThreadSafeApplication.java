package com.pres.cf.threadsafe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ThreadSafeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThreadSafeApplication.class, args);
    }

}

package com.pres.cf.threadsafe.arraylist;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Dora
 * @date 2019/10/22 10:13
 **/
@Slf4j
public class ArrayListTest {
    public static void main(String[] args) {
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("1");
        objects.add("2");
        objects.add("3");
        objects.add(null);
        int size = objects.size();
        log.warn("原来长度为:{}", size);
        objects.trimToSize();
        size = objects.size();
        log.warn("去掉空格长度为:{}", size);
        ArrayList<Object> objects1 = new ArrayList<>(objects);
        log.warn("复制一份长度为:{}", objects1.size());

        // arrayList的线程安全版本
        CopyOnWriteArrayList lockObjects = new CopyOnWriteArrayList();


        HashMap map = new HashMap();

    }
}

package com.pres.cf.threadsafe.demo.deadlock;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora
 * @date 2019/9/30 14:21
 **/
@Slf4j
public class Thread1 implements Runnable {
    private Object lock2;
    private Object lock1;

    public Thread1(Object lock1, Object lock2) {
        this.lock1 = lock1;
        this.lock2 = lock2;
    }

    @SneakyThrows({InterruptedException.class})
    @Override
    public void run() {
        synchronized (lock1) {
            log.warn("thread 1:begining...");
            Thread.sleep(10);
            log.warn("thread 1:waiting for lock...");
            synchronized (lock2) {
                log.warn("thread 1:holding lock 1 & 2");
            }
        }

    }
}

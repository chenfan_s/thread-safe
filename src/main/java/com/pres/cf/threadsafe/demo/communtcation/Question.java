package com.pres.cf.threadsafe.demo.communtcation;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora 线程之间的通信
 * @date 2019/9/30 11:13
 **/
@Slf4j
public class Question implements Runnable {
    Chat msg;
    private String[] msgArray = {"Hi", "How are you?", "I am also doing fine!"};

    public Question(Chat msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        for (int i = 0; i < msgArray.length; i++) {
            msg.question(msgArray[i]);
        }
    }


}

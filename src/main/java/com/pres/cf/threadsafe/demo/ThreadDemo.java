package com.pres.cf.threadsafe.demo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora thread 实现线程
 * @date 2019/9/30 9:47
 **/
@Slf4j
public class ThreadDemo extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            log.warn("i:{}", i);
        }
    }
}

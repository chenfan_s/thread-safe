package com.pres.cf.threadsafe.demo.communtcation;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora
 * @date 2019/9/30 11:48
 **/
@Slf4j
public class Answer implements Runnable {
    Chat msg;
    String[] msgArrays = {"Hi", "I am good,and you", "Great !"};

    public Answer(Chat msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        for (int i = 0; i < msgArrays.length; i++) {
            msg.answer(msgArrays[i]);
        }
    }
}

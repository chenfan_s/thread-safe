package com.pres.cf.threadsafe.demo;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora   挂起  线程被挂起
 * @date 2019/9/30 10:02
 **/
@Slf4j
public class SuspendDemo implements Runnable {
    // suspend :false  没有被挂起
    private Boolean suspend = false;

    @Override
    @SneakyThrows({InterruptedException.class})
    public void run() {
        for (int i = 0; i < 10; i++) {
            log.warn("i:{}", i);
            Thread.sleep(500);
            // 这条线程被挂起
            synchronized (this) {
                while (suspend) {
                    if (log.isWarnEnabled()) {
                        log.warn("suspend:{}", Thread.currentThread().getName());
                    }
                    wait();
                }
            }
        }

    }

    /**
     * 线程被挂起
     * 如果这里不加 synchronized 会抛出IllegalMonitorStateException 异常
     * // 唤醒  是由这条任务执行的主线程去唤醒子线程
     *
     * @return
     * @author dora
     * @date 2019/9/30
     **/
    public synchronized void resume() {
        suspend = false;
        notify();
        if (log.isWarnEnabled()) {
            log.warn("notify:{}", Thread.currentThread().getName());
        }
    }

    /**
     * 挂起线程
     *
     * @return
     * @author dora
     * @date 2019/9/30
     **/
    public void suspend() {
        suspend = true;
    }


}

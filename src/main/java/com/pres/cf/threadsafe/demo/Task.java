package com.pres.cf.threadsafe.demo;

import com.pres.cf.threadsafe.demo.communtcation.Answer;
import com.pres.cf.threadsafe.demo.communtcation.Chat;
import com.pres.cf.threadsafe.demo.communtcation.Question;
import com.pres.cf.threadsafe.demo.deadlock.Thread1;
import com.pres.cf.threadsafe.demo.deadlock.Thread2;
import com.pres.cf.threadsafe.demo.lock.LockDemo;
import com.pres.cf.threadsafe.demo.lock.LockPrintDemo;
import com.pres.cf.threadsafe.demo.lock.readwritelock.TestTrhead;
import com.pres.cf.threadsafe.demo.synchronize.PrintDemo;
import com.pres.cf.threadsafe.demo.synchronize.ThreadPrintDemo;
import com.pres.cf.threadsafe.demo.threadlocal.ThreadLocalDemo;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Dora
 * @date 2019/9/30 9:28
 **/
@Component
@Slf4j
public class Task {
    @SneakyThrows({InterruptedException.class})
    //@Scheduled(cron = "0/5 * * ? * * ")
    public void job1() {
        for (int i = 0; i < 3; i++) {
            // 我想要的是 按顺序输出 显然输出结果 跟我要的答案不一致, 多线程的3大:原子性,有序性,可见性
            // 在这里我要解决的是有序性
            Thread thread = new Thread(new RunnableThread(), "Thread-" + i);
            thread.start();
            // join方法在一定程度上是用来解决线程的有序性的
            thread.join();
        }
    }

    @SneakyThrows({InterruptedException.class})
    //  @Scheduled(cron = "0/5 * * ? * * ")
    public void job2() {
        for (int i = 0; i < 3; i++) {
            ThreadDemo threadDemo = new ThreadDemo();
            threadDemo.start();
            threadDemo.join();
        }
    }

    @SneakyThrows({InterruptedException.class})
    public void job3() {
        SuspendDemo demo = new SuspendDemo();
        Thread t1 = new Thread(demo, "suspend");
        t1.start();
        Thread.sleep(1000);
        // 挂起线程
        demo.suspend();
        Thread.sleep(500);
        // 唤醒线程 :由执行当前定时任务的线程  唤醒当前对象的线程
        demo.resume();
    }


    //  @Scheduled(fixedDelay = 50000)
    public void jobChat() {
        // 模拟对话场景  重点:线程之间的通讯机制
        // 线程1-提问,线程2-回答 共享1个对话场景 .
        Chat chat = new Chat();
        Thread question = new Thread(new Question(chat), "question-thread");
        question.start();
        Thread answer = new Thread(new Answer(chat), "answer-thread");
        answer.start();
    }

    @SneakyThrows({InterruptedException.class})
    //   @Scheduled(cron = "0/5 * * ? * * ")
    public void job4() {
        log.info("重头开始啦-----------------");
        PrintDemo printDemo = new PrintDemo();
        ThreadPrintDemo threadPrintDemo = new ThreadPrintDemo(printDemo);
        for (int i = 0; i < 3; i++) {
            Thread thread = new Thread(threadPrintDemo, "Thread-print-" + i);
            thread.start();
            thread.join();
        }
    }

    //   @Scheduled(fixedDelay = 50000)
    public void job5() {
        // 死锁
        // 产生的实现: 有两把锁,锁1和锁2 ,有两条线程 分别是线程1和线程2
        // 线程1 和线程2 公用两把锁,线程1 先锁住锁1,锁2后锁住,线程2反之,先锁住锁2 再锁住锁1
        // 同时运行 线程1要等着锁2被释放,操作锁2,线程2要等着锁1被释放,操作锁1,两者之间就产生了死锁
        // 死锁的解决方法:两条线程之间的锁的拿取顺序相同
        Object o1 = new Object();
        Object o2 = new Object();
        Thread1 thread1 = new Thread1(o1, o2);
        Thread t1 = new Thread(thread1, "thread-1");
        t1.start();
        Thread2 thread2 = new Thread2(o1, o2);
        Thread t2 = new Thread(thread2, "thread-1");
        t2.start();
    }


    // @Scheduled(fixedDelay = 50000)
    @SneakyThrows({InterruptedException.class})
    public void job6() {
        // threadlocal的使用
        // threadlocal是每个线程自己带有的,线程销毁,threadlocal也会跟着销毁
        ThreadLocalDemo threadLocalDemo = new ThreadLocalDemo();
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(threadLocalDemo, "thread-" + i);
            thread.start();
            thread.join();
        }
    }

    // @Scheduled(fixedDelay = 50000)
    public void job7() {
        // 有关于线程安全的随机数
        // random 和ThreadLocalRandom 都是线程安全的.但是在多线程环境下Random的性能不高,所以才有了ThreadLocalRandom
        System.out.println("Random Integer: " + new Random().nextInt());
        System.out.println("Seeded Random Integer: " + new Random(15).nextInt());
        System.out.println("Thread Local Random Integer: " + ThreadLocalRandom.current().nextInt());
        final ThreadLocalRandom random = ThreadLocalRandom.current();
        //    random.setSeed(15);
        System.out.println("Seeded Thread Local Random Integer: " + random.nextInt());

        System.out.println("local random Integer: " + random.nextInt(1, 100));
    }


    public void job8() {
        // 用lock 实现线程同步代码
        LockDemo demo = new LockDemo();
        LockPrintDemo printDemo = new LockPrintDemo(demo);
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(printDemo, "Thread-" + i);
            thread.start();

        }
    }

    @Scheduled(fixedDelay = 50000)
    public void job9() {
        // 读写锁
        TestTrhead testTrhead = new TestTrhead();
        testTrhead.test();
    }




}

package com.pres.cf.threadsafe.demo.synchronize;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora
 * @date 2019/9/30 14:07
 **/
@Slf4j
public class ThreadPrintDemo implements Runnable {
    private PrintDemo printDemo;

    public ThreadPrintDemo(PrintDemo printDemo) {
        this.printDemo = printDemo;
    }

    @Override
    public void run() {
        //    synchronized (printDemo){
        printDemo.printCount();
        // }

    }
}

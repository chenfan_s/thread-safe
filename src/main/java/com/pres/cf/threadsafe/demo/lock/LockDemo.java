package com.pres.cf.threadsafe.demo.lock;

import lombok.SneakyThrows;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Dora
 * @date 2019/10/8 11:17
 **/
public class LockDemo {
    private Lock queueLock = new ReentrantLock();

    @SneakyThrows({InterruptedException.class})
    public void print() {
        // 加锁
        queueLock.lock();
        Long duration = (long) (Math.random() * 10000);
        System.out.println(Thread.currentThread().getName()
                + "  Time Taken " + (duration / 1000) + " seconds.");
        Thread.sleep(duration);
        System.out.printf("%s printed the document successfully.\n", Thread.currentThread().getName());
        // 解锁
        queueLock.unlock();

    }
}

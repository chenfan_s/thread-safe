package com.pres.cf.threadsafe.demo.threadlocal;

import java.util.Objects;

/**
 * @author Dora
 * @date 2019/10/8 10:18
 **/
public class ThreadLocalDemo implements Runnable {
    int counter;

    ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    @Override
    public void run() {
        counter++;
        if (!Objects.isNull(threadLocal.get())) {
            threadLocal.set((threadLocal.get().intValue() + 1));
        } else {
            threadLocal.set(0);
        }

        System.out.println("counter = " + counter);
        System.out.println("threadLocal = " + threadLocal.get() + "----threadName---" + Thread.currentThread().getName());

    }
}

package com.pres.cf.threadsafe.demo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora runnable实现线程
 * @date 2019/9/30 9:13
 **/
@Slf4j
public class RunnableThread implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            log.warn("i:{}", i);
        }
    }


}

package com.pres.cf.threadsafe.demo.lock;

/**
 * @author Dora
 * @date 2019/10/8 11:44
 **/
public class LockPrintDemo implements Runnable {
    private LockDemo lockDemo;

    public LockPrintDemo(LockDemo lockDemo) {
        this.lockDemo = lockDemo;
    }

    @Override
    public void run() {
        System.out.printf("%s starts printing a document\n", Thread.currentThread().getName());
        lockDemo.print();
    }
}

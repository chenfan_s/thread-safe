package com.pres.cf.threadsafe.demo.synchronize;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora
 * @date 2019/9/30 14:03
 **/
@Slf4j
public class PrintDemo {
    public void printCount() {
        for (int i = 0; i < 5; i++) {
            if (log.isWarnEnabled()) {
                log.warn("i:{}", i);
            }
        }
    }
}

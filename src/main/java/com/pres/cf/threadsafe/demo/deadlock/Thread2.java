package com.pres.cf.threadsafe.demo.deadlock;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora
 * @date 2019/9/30 14:27
 **/
@Slf4j
public class Thread2 implements Runnable {
    private Object lock2;
    private Object lock1;

    public Thread2(Object lock1, Object lock2) {
        this.lock1 = lock1;
        this.lock2 = lock2;
    }

    @SneakyThrows({InterruptedException.class})
    @Override
    public void run() {
        synchronized (lock2) {
            log.warn("thread 2:begining...");
            Thread.sleep(10);
            log.warn("thread 2:waiting for lock...");
            synchronized (lock1) {
                log.warn("Thread 2:holding lock 2 & 1");
            }
        }

    }
}

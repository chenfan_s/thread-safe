package com.pres.cf.threadsafe.demo.communtcation;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dora
 * @date 2019/9/30 11:27
 **/
@Slf4j
public class Chat {
    boolean flag = false;

    @SneakyThrows({InterruptedException.class})
    public synchronized void question(String msg) {
        if (flag) {
            wait();
            if (log.isInfoEnabled()) {
                log.info("question-wait:{}", Thread.currentThread().getName());
            }

        }
        if (log.isWarnEnabled()) {
            log.warn("question:{}", msg);
        }
        flag = true;
        notify();
    }

    @SneakyThrows({InterruptedException.class})
    public synchronized void answer(String msg) {
        if (!flag) {
            wait();
            if (log.isInfoEnabled()) {
                log.info("answer-wait:{}", Thread.currentThread().getName());
            }
        }
        if (log.isWarnEnabled()) {
            log.warn("answer:{}", msg);
        }
        flag = false;
        notify();

    }


}

package com.pres.cf.threadsafe;

import com.pres.cf.threadsafe.demo.RunnableThread;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Dora
 * @date 2019/9/30 9:19
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class RunnableTests {

    @Test
    @SneakyThrows({InterruptedException.class})
    public void t1() {
        for (int i = 0; i < 3; i++) {
            // 我想要的是 按顺序输出 显然输出结果 跟我要的答案不一致, 多线程的3大:原子性,有序性,可见性
            // 在这里我要解决的是有序性
            Thread thread = new Thread(new RunnableThread(), "runnable-Thread-" + i);
            thread.start();
            // join方法是用来解决线程的有序性的
            thread.join();
        }


    }


}
